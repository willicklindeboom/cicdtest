import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class HelloWorldTest {

    @Test
    void test(){
        HelloWorld mock = Mockito.mock(HelloWorld.class);
        when(mock.returnName()).thenReturn("Bob");
        HelloWorld helloWorld = new HelloWorld();
        assertEquals("Bob",helloWorld.returnName());

    }
}